## 課題提出
必須の課題は次の通りです。(増えます)

* Linux 標準教科書の各章末問題
* VirtualBox の仮想マシンでフルスクリーンで動くようにする
* 自宅のパソコンで Ubuntu の環境を整える
* GitLab のプロフィール画像・名前をわかりやすいものに (ただし、ネットで公開するものなので、気になる人は写真や本名は避けてください)
* Markdown の練習
* Wiki の練習
* 質問を最低一つ
* GitLab でプロジェクトを作る
* インストールした Ubuntu の iso イメージファイルが本物かの証明


### 提出方法
課題の提出及レビューは[イシューボード](https://gitlab.com/harada-22/home/-/boards)で行います。

1. 課題を始める前に、[課題のイシューを作成](https://gitlab.com/harada-22/home/-/issues/new)します 
   - Title と説明はわかりやすく簡潔に (特に Title)
   - ラベル ~"課題" をイシューに追加します
   - 担当者をあなた自身に変更します
2. 直近でやる課題は[イシューボード](https://gitlab.com/harada-22/home/-/boards)で ~"To Do" に移動してください
3. 課題を始める前に[イシューボード](https://gitlab.com/harada-22/home/-/boards)で ~"Doing" に移動してください
4. 課題をこなしたら、コメントにその旨記載して、@masakura とメンションを加えた上で、[イシューボード](https://gitlab.com/harada-22/home/-/boards)で ~"提出済" に移動してください
   - Linux の標準教科書のような章末問題が複数ある場合、回答はコメントにひとつづつ書いてください
   - Paiza の問題などのほかの課題をクリアしたときは、証拠となるスクリーンショットを貼り付けるなどして、完了したのがわかるようにしてください

提出は以上です。内容に問題がないか @masakura で確認の上、Closed へ移動します。そしたら課題完了です。

### 再提出
提出された課題の内容に間違いがあったり、詳しく聞きたいことがある場合は、コメントのスレッドで @masakura が質問などを書いたうえでラベル ~"再提出" をつけます。

間違いの訂正や質問への回答は、そのコメントのスレッドに書き込んでください。以前のコメントを修正するのはどこを間違ったのかわからなくなりますので避けてください。その際、再度 @masakura にメンションをすることと、ラベル ~"再提出" を外すことを忘れないでください。 


### 課題の種類
課題はゼミで出す必須の課題のほかに、自分が頑張ったこと、やってみたことを提出する自由課題があります。

* ゼミで使っている以外の Linux ディストリビューションや仮想化ソフトウェアを使ってみた
* VirtualBox の便利な機能を使った
  - コピペ
  - スクリーンショット
  - スナップショット
  - マイクとサウンドを使って、Teams でテレビ会議をした
  - etc...
* GitLab を日本語化した
* パスワードマネージャーを使った
* Paiza の課題を解いた
* 必須ではない手順書を書いた
* 手順書の間違いを発見して直した or 直してもらった
* タッチタイピングゲームで高得点を出した
* Ubuntu でいろんなことをしてみた
* プログラムを書いてみた
* 入門書を読んだ
* 雑誌を読んだ
* 勉強会に出た
* etc...

提出して評価がマイナスになることはないので、何か頑張ったと思ったらなんでも提出してください。

ただし、次のものは提出物として認めません。

* 他の授業で課題として提出したもの (ただし、提出後にさらに改造したプログラムなどは、改造分だけの提出を認めます)
* 他人への誹謗中傷や、法律・マナー違反など


## 手順書の作成
手順書の作成も課題の提出とやり方は同じです。

* ラベルは ~"課題" だけでなく、~documentation も追加してください
* 手順書は [Wiki](https://gitlab.com/harada-22/home/-/wikis/home) に書いたうえで、提出の際にはイシューの説明に Wiki へのリンクを含めてください。


## 質問
質問も課題の提出とやり方は似ています。

* ラベル ~"課題" ではなく、ラベル ~"質問" をつけてください
* 説明に他人でもわかるように質問内容を記載してください
* 担当者は @masakura にしてください
* [イシューボード](https://gitlab.com/harada-22/home/-/boards) で ~Doing に移動してください

あなたが疑問に思ったことは他の人も疑問に思ってる可能性が高いです。自分のためだけでなく、他の人にもメリットがありますので、じゃんじゃん質問しましょう。

* 質問の前に[イシュー](https://gitlab.com/harada-22/home/-/issues/?sort=created_date&state=all&first_page_size=20)の一覧で、類似の質問がないか検索してください。類似の質問があった場合は、私も! などとコメントに書き込んでください。
* [技術系メーリングリストで質問するときのパターン・ランゲージ](https://www.hyuki.com/writing/techask.html)を一読してください
  - 動作環境などはゼミで使っている Ubuntu 環境の限り、記載しなくても OK です
  - 記載が足りないところは @masakura のほうでその旨伝えますので、追記してください

## 資料など
* [Linux標準教科書](https://linuc.org/textbooks/linux/)
* [Ubuntu Japanese Team](https://www.ubuntulinux.jp/)
* [Oracle VM VirtualBox User Manual](https://download.virtualbox.org/virtualbox/6.1.40/UserManual.pdf)
* [技術系メーリングリストで質問するときのパターン・ランゲージ](https://www.hyuki.com/writing/techask.html)
* GitLab 関係
  - [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html) - GitLab での Markdown の書き方
  - [イシューの管理](https://gitlab-docs.creationline.com/ee/user/project/issues/managing_issues.html)
