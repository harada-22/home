function add(x, y) {
  return x + y;
}

describe('テストサンプル', () => {
  it('足し算', () => {
    expect(add(1, 2)).toEqual(3);
  });
});
